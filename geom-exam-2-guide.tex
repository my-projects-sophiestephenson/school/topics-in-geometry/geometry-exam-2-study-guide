\documentclass[12pt, twoside]{amsart}

\newcommand{\R}{{\mathbb R}}
\newcommand{\Rbar}{\overline{\R}}
\newcommand{\Z}{{\mathbb Z}}
\newcommand{\C}{{\mathbb C}}
\newcommand{\Cp}{\C^+}
\newcommand{\xn}{{\vec x_n}}
\newcommand{\M}{\mathscr M}
\newcommand{\nin}{\noindent}
\newcommand{\ul}{\underline}
\newcommand{\inv}{^{-1}}
\newcommand{\bigH}{\mathbb{H}}

\setlength{\textwidth}{6.5in}
\setlength{\textheight}{8.5in}
\setlength{\oddsidemargin}{0in}
\setlength{\evensidemargin}{0in}
\setlength{\topmargin}{0in}

\everymath{\displaystyle}
\usepackage{commath}
\usepackage[mathscr]{euscript}


\begin{document}


\centerline{\large \sc Geometry - Exam 2 Study Guide}
\bigskip
\bigskip

\nin {\large \bf Wednesday, October 2}\\

\nin \ul{Definition:} \textbf{Mobius geometry} is $\Cp$ along with $\M$, the set of transformations $T: \Cp \rightarrow \Cp$ such that $T(z) = \frac{az + b}{cz + d}$ for $a, b, c, d \in \C$ and $ad - bc \not = 0$.\\

\nin Ex) A rotation about 0 has the form $T(z) = e^{i\theta}z = \frac{az + b}{cz + d}$ for $a = e^{i\theta}, b = c = 0, d = 1$.\\
\nin Ex) Reflections about 0 are also in $\M: T(z) = e^{i \theta}\bar{z}$.\\

Different values of $a, b, c, d$ may yield the same transformation; in fact, if $T(z) = \frac{az + b}{cz + d}$  and $ \alpha \in \C \not = 0$, then $T(z) = \frac{\alpha a z + \alpha b}{\alpha c z + \alpha d}$. In fact, there are infinite values of $a, b, c, d$ which represent the same transformation.\\

\nin \ul{Theorem:} $\M$, the set of Mobius transformations, forms a group because:
\begin{enumerate}
	\item[i.)] $T(z) = z \in \M$
	\item[ii.)] If $S, T \in \M, S \circ T \in \M$
	\item[iii.)] If $S \in \M$, $S^{-1} \in \M$\\
\end{enumerate}
\bigskip
\bigskip

\nin {\large \bf Friday, October 4}\\

\nin \ul{Claim:} $M_{T^{-1}} = M_T^{-1}$.\\

\nin \ul{Definition:} Let $\overline{\M} = \{ T(z) = \frac{az + b}{cz + d} \text{ or } \frac{a\bar{z} + b}{c\bar{z} + d} \mid ad - bc \not = 0\}$.\\ \\

If $T \in \overline{\M}$, with $T: \Cp \rightarrow \Cp $, then $T(\infty) = \lim_{z \rightarrow \infty} T(z)$. Generally, when we define $T(\infty)$, we have three cases if $T(z) = \frac{az + b}{cz + d}$:
\begin{enumerate}
	\item[i.] Suppose $a, c \not = 0$. Then, $T(\infty) = \lim_{z \rightarrow \infty} \frac{az + b}{cz + d} = \frac{a}{c}$.
	\item[ii.] Suppose $c = 0, a \not = 0$. Then, $T(z) = \frac{az + b}{d}$ and $T(\infty) = \infty$.
	\item[iii.] Suppose $c \not = 0, a = 0$. Then, $T(z) = \frac{b}{cz + d}$ and $T(\infty) = 0$.
\end{enumerate}
What if $T(z) = \frac{0}{0}$? This can't happen - the determinant would be zero.\\

\nin Recall: Let $T \in \overline{\M}$. Then $c$ is a {\bf fixed point} of $T$ if $T(c) = c$. The number of fixed points of $T$ tells you about how $T$ acts on $\Cp$.\\

\nin \ul{Theorem:} If $T$ has 3 or more fixed points, then $T(z) = z$.\\

\nin Ex) $T(z) = 7z + 6$. By inspection, $T(-1) = -1$ and $T(\infty) = \infty$. Thus, the fixed points are $z = -1, \infty$.\\

\bigskip
\bigskip

\nin {\large \bf Friday, October 11}\\
Invariants of the Mobius Group\\

\nin \ul{Definition:} A {\bf circle in $\Cp$} - also known as a {\bf cline} - is either an actual circle in $\C$ or a line in $\C$. (In $\Cp$, the line $l$ in $\C$ is really $l \cup \{ \infty \}$.)\\

\nin \ul{Theorem:} Any circle in $\Cp$ has the form $\alpha z \bar{z} + \beta z + \overline{\beta}\bar{z} + \gamma = 0$. If $\alpha = 0$, this describes a line in $\C$; otherwise, it describes a circle in $\C$. Here, $\alpha, \gamma \in \R$ and $\beta \in \C$.\\

\nin \ul{Theorem:} Clines are invariants in Mobius geometry. That is, for any $T \in \M$, $T$ maps clines to clines.

\begin{proof} Suppose $z$ satisfies $\alpha  \bar{z} + \beta z + \overline{\beta}\bar{z} + \gamma = 0$ with $\alpha, \gamma \in \R$ and $\beta \in \C$. Let $T(z) = \tfrac{1}{z} = w$. Thus, we have $z = \tfrac{1}{w}$. We plug $\tfrac{1}{w}$ into the equation and see that it produces another cline.\\
\end{proof}

\nin \ul{Definition:} Another invariant of the Mobius Group is the {\bf cross ratio}. The cross ratio $(z_0, z_1, z_2, z_3)$ for $z_0, z_1, z_2, z_3 \in \C$ is defined as 
$$\frac{(z_0 - z_2)(z_1 - z_2)}{(z_0 - z_3)(z_1 - z_2)}.$$\\

\nin \ul{Theorem:} The cross ratio $(z_0, z_1, z_2, z_3)$ is a real number iff $z_0, z_1, z_2, z_3$ lie on a cline in $\Cp$.\\

\nin \ul{Theorem:} If $T \in \M$, then $T$ preserves the cross ratio, i.e.
$$(z_0, z_1, z_2, z_3) = (T(z_0), T(z_1), T(z_2), T(z_3)).$$\\

\nin \ul{Corollary:} If $T \in \M$, $T$ sends clines to clines.
\begin{proof} We already knew this, but now we can use the above to prove it easily.\\ 
\end{proof}

\bigskip
\bigskip

\nin {\large \bf Monday, October 14}\\

\nin \ul{Fundamental Theorem of Mobius Geometry:} There exists a unique $T \in \M$ that takes a triple $z_1, z_2, z_3$ of distinct points to any other triple of distinct points $w_1, w_2, w_3$. We say that $\M$ acts {\bf triply transitively} on $\C$. \\

\begin{proof}[Proof sketch] It suffices to find $T \in \M$ such that $T(z_1) = 1$, $T(z_2) = 0$, $T(z_3) = \infty$. 

\end{proof}

\nin \ul{Proposition:} Any Mobius transformation has 3 fixed points in $\Cp$. \\

\bigskip
\bigskip

\nin {\large \bf Wednesday, October 16}\\

\nin Ex) Let $z_1, z_2, z_3$ be $0, 1,$ and $i$, and $w_1, w_2, w_3$ are $-1, 1,$ and $2i$. 
We have that 
$$T(z) = \frac{(z - 1)(-i)}{(z - i)(-1)} = \frac{zi -i}{z - i}.$$
The matrix for $T$ is $M_T = \begin{pmatrix} i & -i \\ 1 & -i \end{pmatrix}$. Also, 
$$S(z) = \frac{(z - 1)(-1 -2i)}{(z - 2i)(-1 -i)} = \frac{(1/2 + i)z - (1/2 + i)}{z - 2i},$$
and the matrix for $S$ is $M_S = \begin{pmatrix} 1/2 + i & -1/2 - i \\ 1 & -2i \end{pmatrix}$.
The determinant of $S$ is $(1/2 + i)(-2i + 1)$, so 
$$M_{S^-1} = \frac{1}{(1/2 + i)(-2i + 1)} \begin{pmatrix} -2i & 1/2 + i \\ -1 & 1/2 + i \end{pmatrix} = \begin{pmatrix} -2i & 1/2 + i \\ -1 & 1/2 + i \end{pmatrix}.$$\\
(We can forget about multiplying by the scalar because it makes no difference to the transformation.)
We see that 
$$M_{(S^{-1} \circ T)} = M_S^{-1}M_T = \begin{pmatrix} -2i & 1/2 + i \\ -1 & 1/2 + i \end{pmatrix}\begin{pmatrix} i & -i \\ 1 &-i \end{pmatrix} = \begin{pmatrix} 5/2 + i & -1 - 1/2i \\ 1/2 & 1 + 1/2i\end{pmatrix}.$$
This is a transformation that maps 0 to 1, 1 to 0, and $i$ to $\infty$.\\

\nin \ul{Theorem:} The cross ratio is invariant under Mobius transformations.
\begin{proof} Let $S \in \M = S(z) = (z, z_1, z_2, z_3)$. $S(z_1) = 1$, $S(z_2) = 0$, and $S(z_3) = \infty$. Let $T \in \M$ be arbitrary. Then, 
$$ST\inv T(z_1) = 1, \ \ \ ST\inv T(z_2) = 0, \ \ \ ST\inv T(z_3) = \infty.$$
So, 
$$(z, z_1, z_2, z_3) = S(z) = ST\inv T(z) = (z, T(z_1), T(z_2), T(z_3)).$$\\
\end{proof}

\nin \ul{Theorem:} $(z_0, z_1, z_2, z_3) \in \R \iff z_0, z_1, z_2, z_3$ lie on a cline.\\
\bigskip
\bigskip

\nin {\large \bf Friday, October 18}\\
Hyperbolic Geometry\\

\nin \ul{Definitions:}
\begin{enumerate}
	\item[i.] The {\bf upper-half plane}, denoted $\bigH$, is defined as $\{ z \in \C \mid Im(z) > 0\}$.\\
	\item[ii.] The {\bf boundary at infinity}, denoted $\Rbar$, is the real axis $\cup \{ \infty \}$.\\
	\indent Note: $\bigH \cup \Rbar \subseteq \Cp$.\\
	\item[iii.] The Isometry group of $\bigH$ consisting of only orientation-preserving isometries is $Isom^+ (\bigH)$.\\
	\item[iv.] A {\bf geodesic} is a length-minimizing path between two points. In $\bigH$, a geodesic is either a vertical line or a semicircle perpendicular to the real axis. \\
	\item[v.] A semicircle $C$ is {\bf perpendicular} to the real axis if at the point of intersection with the real axis, the tangent line of $C$ is vertical. It also means that the center is on the real axis.\\
\end{enumerate}

\nin \ul{Remark:} In $\R^2$, given any line $l$ and any point $p$ not on the line, there exists a unique line $l'$ such that $p$ is on $l'$ and $l$ and $l'$ are parallel. This is {\em false} in $\bigH$. In $\bigH$, two lines are parallel if they don't intersect in $\bigH$. (They could intersect on the real axis.) For any $l$ and $p$ in $\bigH$, there are infinite lines $l'$ that are parallel to $l$ and pass through $p$.\\

Since $\bigH \cup \Rbar \subseteq \Cp$, we have that $Isom^+(\bigH) \subseteq \M$. In fact, $Isom^+(\bigH)$ is the subset of $\M$ consisting of all $T: \bigH \rightarrow \bigH$ and $T: \Rbar \rightarrow \Rbar$. What form do these transformations take?\\

\nin \ul{Theorem:} $Isom^+(\bigH) = \{ T(z) = \frac{az + b}{cz + d} \mid a, b, c, d \in \R \text{ and } ad - bc = 1 \}$.\\

To measure distance between two points, the metric in $\bigH$ is {\em not} the same as the metric in $\C$ or $\R^2$. For two points on the imaginary axis $ai$ and $bi$ such that $b > a$, we have that $$d_{\bigH} (ai, bi) = \ln (\tfrac{b}{a}).$$
Notice that $\lim_{r \rightarrow 0}d_{\bigH}(i, ri) = \lim_{r \rightarrow 0} \ln(r) = \infty.$ Thus, every real number is infinitely far from any point in $\bigH$. This is why we call $\Rbar$ the boundary at infinity: all $x \in \Rbar$ are points at infinity, meaning there are uncountably many points at infinity. Also, for two points $z, w \in \bigH$ with $z > w$, we have that for any $x > 0$, 
$$d_{\bigH}(z, w) < d_{\bigH}(z - x, w - x).$$
In fact, shifting points downward in $\bigH$ makes the distance between them exponentially larger.\\ 

\bigskip
\bigskip

\nin {\large \bf Monday, October 28}\\

\nin \ul{Claim:} A Euclidean circle is perpendicular to the real axis if and only if its center is on the real axis. \\

\nin \ul{Theorem:} Any two points can be connected by some line segment in $\bigH$ (a hyperbolic line) which is unique. (I.e., Euclid's first postulate holds for $\bigH$.)
\begin{proof} Consider $p, q \in \bigH$. If $Re(p) = Re(q)$, then the vertical line between them is the unique hyperbolic line connecting $p$ and $q$. If $Re(p) \not = Re(q)$, then we can construct a circle perpendicular to the real axis that passes through $p$ and $q$. To do this, we go through the following steps:
\begin{enumerate}
	\item[1.] Draw a Euclidean line segment between the $p$ and $q$. 
	\item[2.] Draw the perpendicular bisector of this line segment. The point where the bisector intersects $\R$ is the center of the circle we want; call it $c$.
	\item[3.] The radius of the circle is the distance from $c$ to either $p$ or $q$ (they are equal).
	\item[4.] The equation of the circle is $|z - c| = |c - p| = |c - q|$.
\end{enumerate}
You can check that $p$ and $q$ lie on this circle, and by construction, it is unique.\\
\end{proof}

\nin It turns out that all of Euclid's postulates hold in $\bigH$ - all except the 5th. \\

\nin \ul{Playfair's axiom (equiv. to Euclid's 5th postulate):} Given a line$l$ and some point $p$ not on $l$, there exists a unique line passing through the point $p$ that doesn't intersect the line $l$. \\

\nin \ul{Theorem:} Let $l$ be a hyperbolic line, $p \in \bigH$ with $p$ not on $l$. Then there exist infinitely many hyperbolic lines parallel to $l$ which pass through $p$. 

\begin{proof} 
\begin{enumerate}
	\item[i.] If $l$ is a vertical line, then let $l'$ be the vertical line passing through $p$. Certainly, $l$ is parallel to $l'$. Let $x$ be a point on $\R$ between $l$ and $l'$. We know that for such $x$, there exists a semicircle with center on the real axis passing through $p$ with left endpoint being $x$. There are infinitely many such $x$, so there are infinite semicircles parallel to $l$ passing through $p$.\\
\end{enumerate}
\end{proof}

\bigskip
\bigskip

\nin {\large \bf Wednesday, October 30}\\

\nin \ul{Definition:} Let $l$ be a hyperbolic line. If $l$ is a vertical line, $Re(z) = \alpha$ with $\alpha \in \R$, then one {\bf endpoint at infinity} is $\alpha$ and the other is $\infty$. If $l$ is a semicircle perpendicular to $\R$, then its endpoints at infinity are the places where $l$ would intersect $\R$.\\ 

\nin \ul{Proposition:} If $p \in \bigH$ and $q \in \Rbar$, there exists a unique geodesic passing through $p$ with endpoint at infinity equal to $q$. \\

\nin \ul{Proposition:} Two points $p, q \in \Rbar$ also determine a unique hyperbolic line whose endpoints at infinity are precisely $p$ and $q$. The center of this semicircle is the midpoint between $p$ and $q$, and the radius is $q - $the center.\\

How can we build $Isom^+(\bigH) = \{ T(z) = \frac{az + b}{cz + d} \mid a, b, c, d \in \R \text{ and } ad - bc = 1 \}$, as previously defined?\\

\nin \ul{Fact:} If $T \in \M$ preserves $\Rbar$, then either $T: \bigH \rightarrow \bigH$ or $T : \bigH \rightarrow \{z \in \C \mid Im(z) < 0\}$.\\

This follows from the fact that each $T \in \M$ is continuous on $\Cp$. Thus, to build $Isom^+(\bigH)$, we can first figure out which $T \in \M$ preserve $\Rbar$. \\

First, given $T(z) = \frac{az + b}{cz + d}$, we can associate to $T$ a matrix $M_T = \begin{pmatrix} a' & b' \\ c' & d' \end{pmatrix}$ such that $a'd' - b'c' = 1$. For our $T$, assume $ad - bc = \alpha.$ Then, consider $M_T' = \begin{pmatrix} a/\sqrt{\alpha} & b/\sqrt{\alpha} \\ c/\sqrt{\alpha} & d/\sqrt{\alpha} \end{pmatrix}$. $\det M_T' = ad/\alpha - bc/\alpha = 1$. \\

\nin \ul{Definition:} If $T(z) = \frac{az + b}{cz + d}$ with $ad - bc = 1$, then $T$ is in {\bf normal form}.\\

\nin \ul{Theorem:} Let $T(z) = \frac{az + b}{cz + d} \in \M$. Let $T$ be in normal form. If $T: \Rbar \rightarrow \Rbar$, then either $a, b, c, d \in \R$ or $a, b, c, d$ are purely imaginary. \\

\bigskip
\bigskip

\nin {\large \bf Friday, November 1}\\

\nin \ul{Theorem:} Suppose $T(z) = \frac{az + b}{cz + d}$ with $ad - bc = 1$. Also suppose that $T : \Rbar \rightarrow \Rbar$. If $T: \bigH \rightarrow \bigH$, then $a, b, c, d \in \R$.\\

\bigskip
\bigskip

\nin {\large \bf Monday, November 4}\\

\nin \ul{Parametric Curves}\\

If $C$ is a curve in $\R^2$, then it has a parameterization of the form 
$$\gamma(t) = (x(t), y(t)), \ t \in [a, b ].$$
Parameterizing a curve is helpful because if you can parameterize it, you can find the arc length. \\

\nin Ex) If $C$ is a circle centered at (0, 0) with radius $R$, then the parametric form of the circle is $(R\cos t, R \sin t), \ t \in [0, 2\pi]$.

\nin Ex) Consider the ellipse $\frac{x^2}{a^2} + {y^2}{b^2} = 1$. The parameterization of this ellipse is $(a\cos t, b \sin t)$ for $ t \in [0, 2 \pi ]$.\\

In $\C$ or $\bigH$, the parameterization of a curve $c$ is $z(t) = x(t) + iy(t), \ t \in [a, b]$.\\

\nin \ul{Arc Length in $\R^2$}\\

If $C$ is a curve in $\R^2$, then the arclength of $C$ is $\int_{C} ds$. For example, take $C$ with parameterization $\gamma(t) = (x(t), y(t)), \ t \in [a,b]$.Then, the arc length of $C$ is 
$$\int_a^b \sqrt{(x'(t))^2 + (y'(t))^2} dt.$$
Finding arc length can very quickly become some very gross integration calculations.\\

\nin Ex) Take $C$ = the circle of radius $R$ centered at (0,0). $x(t) = R\cos t, y(t) = R \sin t, t \in [0, 2\pi]$. We have that the arc length is 
$$\int_0^{2\pi} \sqrt{R^2\sin^2 t + R^2 \cos^2 t} dt = \int_0^{2\pi} R dt = Rt \bigg \rvert_0^{2\pi} = 2\pi R.$$
This is to be expected based on our formula for the circumference of a circle.\\

\nin \ul{Arc Length in $\bigH$}\\

Let $C$ be a curve in $\bigH$ with parameterization $z(t) = \cos t + i \sin t, \ t \in [a, b]$. Then, 
$$l_{\bigH}(C) = \int_a^b \frac{|z'(t)|}{y(t)} dt.$$
Note that $z'(t) = x'(t) + iy'(t) \implies |z'(t)| = \sqrt{(x'(t))^2 + (y'(t))^2}.$\\

\nin Ex) Consider $C$ with parameterization $z(t) = it, \ t \in [a, b]$. Then, 
$$l_{\bigH} (C) = \int_a^b \frac{1}{t}dt = \ln t \bigg \rvert_a^b = \ln b - \ln a = \ln (\tfrac{b}{a}),$$
which matches our previously defined formula for distance between two points on the imaginary axis.\\

\bigskip
\bigskip

\nin {\large \bf Wednesday, November 6}\\

\nin \ul{Theorem:} Hyperbolic arc length is invariant under Mobius transformations that preserve the upper half plane, i.e. $\{ T(z)  = \tfrac{az + b}{cz + d} \mid a, b, c, d \in \R \text{ and } ad - bc = 1 \}$.





\end{document}